Mastering CMake
***************

Introduction
============

Mastering CMake is the official book for `CMake`_, a cross-platform,
open-source build system generator.

.. _`CMake`: https://cmake.org

Mastering CMake is maintained and supported by `Kitware`_.

.. _`Kitware`: http://www.kitware.com/cmake
